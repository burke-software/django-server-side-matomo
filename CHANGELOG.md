# 2.1.0

- Added support for django-ipware 3.0

# 2.0.0

- Breaking change - rename piwik to matomo